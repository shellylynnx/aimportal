$(document).ready(function() {
  <!-- When the Menu bar is clicked (have a screen small), toggle navigation items display -->

  $(function() {
    var pull 		= $('#pull');
    menu 		= $('nav ul');
    menuHeight	= menu.height();

    $(pull).on('click', function(e) {
      e.preventDefault();
      menu.slideToggle();
    });
	});

   <!-- When the window is resized larger, after being small, rehide "Menu" -->
   
    $(window).resize(function(){
      var w = $(window).width();
      if(w > 625 && menu.is(':hidden')) {
        menu.removeAttr('style');
      }
    });
   
   
//this displays a confirmation of info saved in Profile forms// 
  $(".saveButtonTop").click(function() {
    document.getElementById("save").style.visibility = "visible";
  });

  $(".saveButtonBottom").click(function() {
    document.getElementById("save2").style.visibility = "visible";
  }); 
 
//  $(".saveButtonSend").click(function() {
//    document.getElementById("send").innerHTML = "A message has been sent to the email address you supplied with details on how to reset your password.";
//  }); 
  
   $(".saveButtonSend").click(function() {
    document.getElementById("send").style.visibility = "visible";
  }); 
 
// highlights box you are in
  $("input").focus (function () {
        $(this).css("outline-color", "#006BB2");
    });
  
});
