$(document).ready(function() {

 // creates accordion on educational resources  page
   $(function() {
      $( "#accordionEd" ).accordion({ collapsible: true, active: false, heightStyle: "content"});
    }); 
    
 // creates accordion on planning  page with just one line.
   $(function() {
      $( "#accordionPlan" ).accordion({ collapsible: true, active: false, heightStyle: "content"});
    }); 
    
/* Trying to remove focus from collapsed accordion header; not working
    $(".ui-accordion-header").click(function(){
           $(this).removeClass("ui-state-focus");     
        });  */
        
 });

